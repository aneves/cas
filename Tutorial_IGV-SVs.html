<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Introduction to IGV -- SVs</title>

<style type="text/css">code{white-space: pre;}</style>
<style type="text/css">
  pre:not([class]) {
    background-color: white;
  }
</style>

<style type="text/css">
h1 {
  font-size: 34px;
}
h1.title {
  font-size: 38px;
}
h2 {
  font-size: 30px;
}
h3 {
  font-size: 24px;
}
h4 {
  font-size: 18px;
}
h5 {
  font-size: 16px;
}
h6 {
  font-size: 12px;
}
.table th:not([align]) {
  text-align: left;
}
</style>


<style type="text/css">
.main-container {
  max-width: 940px;
  margin-left: auto;
  margin-right: auto;
}
code {
  color: inherit;
  background-color: rgba(0, 0, 0, 0.04);
}
img {
  max-width:100%;
  height: auto;
}
.tabbed-pane {
  padding-top: 12px;
}
.html-widget {
  margin-bottom: 20px;
}
button.code-folding-btn:focus {
  outline: none;
}
summary {
  display: list-item;
}
</style>







<body>
<div class="container-fluid main-container">


<!-- setup 3col/9col grid for toc_float and main content  -->

<h1 class="title toc-ignore">Visualisation of SVs</h1>
<h4 class="author">Yann Christinat</h4>
<address class="author_afil">Geneva University Hospitals (HUG)</address>
<h4 class="author">Tutorial adapted from Whalid Gharib (Swiss Institute of Bioinformatics) who adapted it from:<br>
1-Adapted from Griffith lab at the McDonnell Genome Institute, Washington University School of Medicine, St.&nbsp;Louis.<br>
2-Adapted from MRC Clinical Sciences Centre Bioinformatics Team at Imperial College London, Hammersmith Hospital and Mark Dunning of CRUK, Cambridge Institute</h4>
<h4 class="author"><strong><em>This work is shared under Creative Commons Attribution ShareAlike 3.0</em></strong></h4>


<div id="variants-visualisation-in-igv" class="section level1">
<div name="variants_visualisation_in_igv" data-unique="variants_visualisation_in_igv"></div>
<h1>Variants visualisation in IGV</h1>
<p>In this section we will be looking at how IGV can be used for visualizing mutations in sequence data. Here we consider the scenario in which genome sequencing 
has been performed on a DNA sample, sequence reads have been aligned to the reference genome and a variant caller such as GATK HaplotypeCaller or MuTect2 has been run.</p>
<p>We will inspect some regions of the genome where there are possible variants in a breast cancer cell line to determine whether these are real events or artifacts. 
These will include single nucleotide variants (SNVs), small insertions and deletions (indels) and larger structural rearrangements.</p>
<hr>

<h1>IGV setup</h1>
<p>Download and install IGV from the Broad Institute website (<a target="_blank" href="http://software.broadinstitute.org/software/igv/download">http://software.broadinstitute.org/software/igv/download</a>).</p>

<div id="hcc1143-data-set" class="section level3">
<div name="hcc1143_data_set" data-unique="hcc1143_data_set"></div>
<h3>HCC1143 data set</h3>
<p>We will be using publicly available Illumina sequence data generated for the HCC1143 cell line. The HCC1143 cell line was generated from a 52 year old caucasian 
woman with breast cancer. Additional information on this cell line can be found here: <a target="_blank" href="http://www.atcc.org/products/all/CRL-2321.aspx">HCC1143</a> (tumor, 
TNM stage IIA, grade 3, primary ductal carcinoma) and <a target="_blank" href="http://www.atcc.org/products/all/CRL-2362.aspx">HCC1143/BL</a> (matched normal EBV transformed 
lymphoblast cell line).</p>
<p>Sequence reads were aligned to version GRCh37 of the human reference genome. We will be working with subsets of aligned reads in the region: chromosome 21: 19,000,000 - 20,000,000.</p>
<p>The BAM files containing these reads for the cancer cell line and the matched normal are:</p>
<ul>
<li><code>HCC1143.tumour.21.19M-20M.bam</code></li>
<li><code>HCC1143.normal.21.19M-20M.bam</code></li>
</ul>
<p>To download the bam files please click <a href="https://sibcloud-my.sharepoint.com/:f:/g/personal/aitana_lebrand_sib_swiss/EpjnYVFT7ddOvPg_oDDarfkB00c5nLz5K5u0UWaIds4lTg?e=KHNgZ2">here</a></p>
<p>These need to be indexed to be read into IGV. The index files have the .bai suffix and allow IGV to speedily access and display the reads aligning to a specified genomic location.</p>
<p>The reads are from paired end sequencing. DNA fragments of approximately 350 base pairs have been sequenced from each end. The read lengths are 101bp.</p>
<hr>
</div>
<div id="load-aligned-sequence-data" class="section level3">
<div name="load_aligned_sequence_data" data-unique="load_aligned_sequence_data"></div><h3>Load aligned sequence data</h3>
<p>First we need to ensure that IGV is using the same reference genome as that to which the sequence data were aligned, GRCh37, also known as hg19.</p>
<ul>
<li>Select <code>Human hg19</code> from the drop-down list in the top left of the IGV window.</li>
</ul>
<p>Now we’re ready to load the sequence data.</p>
<ul>
<li>Select <code>File &gt; Load from File...</code> from the main menu and select the BAM file <code>HCC1143.normal.21.19M-20M.bam</code> using the file browser.</li>
</ul>
<div class="figure">
<img src="./Tutorial_IGV-images/load_sequence_data.png">
</div>

<hr>
</div>

<h1>Exercices</h1>

<div id="examining-read-alignments" class="section level3">
<div name="examining_read_alignments" data-unique="examining_read_alignments"></div>
<h3>Examining read alignments</h3>
<p>We’re now going to examine read alignments at several genomic loci where there are possible variants.</p>
<p><span class="math display"><span class="MathJax_Preview" style="color: inherit; display: none;"></span><div class="MathJax_Display" style="text-align: center;"><span class="MathJax" id="MathJax-Element-2-Frame" tabindex="0" data-mathml="&lt;math xmlns=&quot;http://www.w3.org/1998/Math/MathML&quot; display=&quot;block&quot; /&gt;" role="presentation" style="text-align: center; position: relative;"><nobr aria-hidden="true"><span class="math" id="MathJax-Span-3" style="width: 0em; display: inline-block;"><span style="display: inline-block; position: relative; width: 0em; height: 0px; font-size: 124%;"><span style="position: absolute; clip: rect(3.805em, 1000em, 4.15em, -999.997em); top: -3.972em; left: 0em;"><span class="mrow" id="MathJax-Span-4"></span><span style="display: inline-block; width: 0px; height: 3.978em;"></span></span></span><span style="display: inline-block; overflow: hidden; vertical-align: -0.068em; border-left: 0px solid; width: 0px; height: 0.146em;"></span></span></nobr><span class="MJX_Assistive_MathML MJX_Assistive_MathML_Block" role="presentation"><math xmlns="http://www.w3.org/1998/Math/MathML" display="block"></math></span></span></div><script type="math/tex; mode=display" id="MathJax-Element-2"> </script></span></p>

<div id="homozygous-deletion" class="section level4">
<h4>Homozygous deletion</h4>
<ul>
<li><p>Navigate to region <code>chr21:19,324,500-19,331,500</code></p></li>
<li>Right click in the main alignment track and select</li>
<li><code>Expanded</code> view</li>
<li><code>View as pairs</code></li>
<li><code>Color alignments by -&gt; insert size and pair orientation</code></li>
<li><p><code>Sort alignments by -&gt; insert size</code></p></li>
<li><p>Hover over one of the red read pairs to display information about the alignments for both ends</p></li>
</ul>
<div class="figure">
<img src="./Tutorial_IGV-images/homozygous_deletion.png">
</div>
<p><strong>Notes</strong></p>
<ul>
<li>The average insert size of a read pair for this sample/library is 350bp</li>
<li>Insert size of red read pairs is 2875bp</li>
<li>This corresponds to a homozygous deletion of 2.5kb</li>
</ul>
<p>Reads that span a rearrangement often have clipped alignments and these can be viewed in IGV.</p>
<ul>
<li><p>Turn off <code>View as pairs</code> and zoom in to the left hand end of the deletion.</p></li>
<li><p>Open the view preferences dialog and select <code>Show soft-clipped bases</code> in the Alignments tab.</p></li>
</ul>
<p><strong>Q1</strong> <em>What do you notice about the clipped sequence from the junction-spanning reads?</em></p>
<p>Repeat for the other end of the deletion.</p>
<hr>
</div>

<div id="loss-of-heterozygosity" class="section level4">
<h4>Loss of heterozygosity</h4>
<p>We’ll load the alignments for the HCC1143 cell line alongside those for the matched normal that we’ve been looking at so far.</p>
<ul>
<li><p>Right click in the main alignment track and turn off <code>View as pairs</code></p></li>
<li><p>Select <code>File &gt; Load from File...</code> from the main menu and select the BAM file <code>HCC1143.tumour.21.19M-20M.bam</code> using the file browser.</p></li>
<li>Zoom out to see a 20kb region surrounding the somatic SNV at <code>chr21:19,544,778</code></li>
<li><p>Select the <code>Collapsed</code> view for both alignment tracks, tumour and normal.</p></li>
</ul>

<div class="figure">
<img src="./Tutorial_IGV-images/loss_of_heterozygosity.png">
</div>
<p><strong>Q2</strong> <em>What do you notice when comparing the variants that are visible in the coverage tracks in the tumour and normal?</em></p>
<hr>
</div>

</div>

</div>

</div>
